﻿using System;
using System.IO;

namespace DataProcessor
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Parsing command line options");

            // Command line validation omitted brevity
            var command = args[0];

            switch (command)
            {
                case "--file":
                    {
                        var filePath = args[1];

                        Console.WriteLine($"Single file {filePath} selected.");

                        ProcessSingleFile(filePath);
                        break;
                    }
                case "--dir":
                    {
                        var directoryPath = args[1];
                        var fileType = GetFileType(args[2]);

                        Console.WriteLine($"Directory {directoryPath} selected for {fileType} files");
                        ProcessDirectory(directoryPath, fileType);
                        break;
                    }
                default:
                    Console.WriteLine("Invalid command line options.");
                    break;
            }

            Console.WriteLine("Press enter to quit.");
            Console.ReadLine();
        }

        private static void ProcessDirectory(string directoryPath, FileType fileType)
        {
        }

        private static void ProcessSingleFile(string filePath)
        {
            var fileProcessor = new FileProcessor(filePath);
            fileProcessor.Process();
        }

        private static FileType GetFileType(string fileType)
        {
            return fileType switch
            {
                "txt" => FileType.Txt,
                _ => throw new ArgumentOutOfRangeException(nameof(fileType))
            };
        }
    }

    public class FileProcessor
    {
        private const string BackupDirectoryName = "backup";
        private const string InProgressDirectoryName = "processing";
        private const string CompleteDirectoryName = "complete";

        public string InputFilePath { get; }

        public FileProcessor(string inputFilePath)
        {
            InputFilePath = inputFilePath;
        }

        public void Process()
        {
            Console.WriteLine($"Begin process of {InputFilePath}.");

            // Check file is exist
            FileExistence(InputFilePath);

            // Get the parent directory
            var directoryInfo = new DirectoryInfo(InputFilePath).Parent;
            if (directoryInfo?.Parent == null) return;
            var rootDirectoryPath = directoryInfo.Parent.FullName;

            Console.WriteLine($"Root data path {rootDirectoryPath}.");

            var backupDirectoryPath = CombineDirectoryPath();

            if (Directory.Exists(backupDirectoryPath)) return;
            Console.WriteLine($"Creating {backupDirectoryPath}");
            Directory.CreateDirectory(backupDirectoryPath);
        }

        private string CombineDirectoryPath()
        {
            //Check directory is exist
            var inputFileDirectoryPath = Path.GetDirectoryName(InputFilePath);
            var backupDirectoryPath = Path.Combine(inputFileDirectoryPath!, BackupDirectoryName);
            return backupDirectoryPath;
        }

        private void FileExistence(string inputFilePath)
        {
            if (!File.Exists(inputFilePath))
                throw new FileNotFoundException($"ERROR: file {inputFilePath} does not exist.");
        }
    }
}